import { NgModule } from '@angular/core';

import { ChartsModule } from 'ng2-charts';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  imports: [
    ThemeModule,ChartsModule
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
