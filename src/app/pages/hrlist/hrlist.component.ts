import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Hritem } from '../../models/hritem';
import { AdalService } from 'adal-angular4';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'hrlist',
  templateUrl: './hrlist.component.html',
  styleUrls: ['./hrlist.component.scss']
})
export class HrlistComponent implements OnInit {
  user: any;
  profile: any;
  constructor(private apiService: ApiService, private adalService: AdalService, protected http: HttpClient) { }
  hrList: Hritem[];
  ngOnInit(){
    console.log("component inited");
    this.apiService.getHrItems().subscribe((res)=>{
        this.hrList = res;
        console.log(JSON.stringify(res));
    });
    this.user = this.adalService.userInfo;

    this.user.token = this.user.token.substring(0, 10) + '...';
  }
  public getProfile() {
    console.log('Get Profile called');
    return this.http.get("https://graph.microsoft.com/v1.0/me");
  }

  public profileClicked() {
    this.getProfile().subscribe({
      next: result => { 
        console.log('Profile Response Received');
        this.profile = result;
      }
    });
  }

}
