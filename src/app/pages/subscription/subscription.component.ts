import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';// ES6 Modules or TypeScript
//import swal from 'sweetalert2'; 
@Component({
  selector: 'subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss'],
  providers: [NgbModalConfig, NgbModal,DecimalPipe,{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class SubscriptionComponent implements OnInit {
   swal: any;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  filter = new FormControl('');
  tabvalue='AWS';
  	// Class variables
     isViewable: boolean; 
      constructor(config: NgbModalConfig, private modalService: NgbModal,_pipe: DecimalPipe,private _formBuilder: FormBuilder) {
    
      config.backdrop = 'static';
      config.keyboard = false;
                                
    }
  ngOnInit() {
    this.isViewable = true;
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }


  onLinkClick(event: MatTabChangeEvent) {
    this.tabvalue=event.tab.textLabel;
  }
 
   addsub(): void { this.isViewable = !this.isViewable; }
   back(){
    this.isViewable = !this.isViewable;
   }
   info(_message: string) {
    //swal.fire('Hello world!')//fire
  }
}
