/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { environment } from '../environments/environment';
import { AdalService } from 'adal-angular4';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  constructor(private analytics: AnalyticsService, private adalService: AdalService) {
    adalService.init(environment.config);
  }

  ngOnInit() {
    this.analytics.trackPageViews();
  }
}
